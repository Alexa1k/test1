package regresin;

import org.junit.jupiter.api.DisplayName;
import org.testng.annotations.Test;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;


public class POSTLoginTest {
    @Test
    @DisplayName("Вызов метода POST /register")
    public void successPostCreateUser() throws IOException, URISyntaxException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI("https://reqres.in/api/register"))
                .POST(HttpRequest.BodyPublishers.ofString("{\n" +
                        "    \"email\": \"sydney@fife\"\n" +
                        "}"))
                .build();

        HttpResponse response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.body());
    }
}
