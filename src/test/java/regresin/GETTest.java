package regresin;

import org.junit.jupiter.api.DisplayName;
import org.testng.annotations.Test;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
public class GETTest {
    @Test
    @DisplayName("Вызов метода GET /ListUsers")
    public void successGetSingleUser() throws IOException {
        StringBuilder result = new StringBuilder();
        URL url = new URL("https://reqres.in/api/users?page=2");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(conn.getInputStream()))) {
            for (String line; (line = reader.readLine()) != null; ) {
                result.append(line);
            }
        }
        System.out.println(result);
    }
}
